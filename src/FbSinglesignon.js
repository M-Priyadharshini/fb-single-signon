import React, { Component } from 'react'

export default class FbSinglesignon extends Component {
    constructor(props){
        super(props)
        this.state={
          user:null
        }
      }
      
    
 fbAsyncInit = function() {
        window.FB.init({
          appId      : 'Enter your AppID from the Facebook Developers Account',
          cookie     : true,                     // Enable cookies to allow the server to access the session.
          xfbml      : true,                     // Parse social plugins on this webpage.
          version    : 'v5.0'                    // Use this Graph API version for this call.
        });
    
      
      (function(d, s, id) {                      // Load the SDK asynchronously
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
           
    }
 


  
    handleFBLogin=()=>{
        window.FB.login(function(response) {
          if (response.authResponse) {
           console.log('Welcome!  Fetching your information.... ');
           window.FB.api('/me', 'GET',
        {"fields":"id,name,email,birthday"},function(response) {
          console.log('Successful login for: ' + response.name,response);
       
        });
        console.log("connected successfully");
          } else {
           console.log('User cancelled login or did not fully authorize.');
          }
        });
             }

    handleSingout=()=>{
      window.FB.logout(function(response) {
       console.log("Logged out");
      });
    }
    render() {
        return (
            <div className="container container-fluid">
                <button type="submit" className="btn btn-primary" onClick={this.handleFBLogin} >Login with Facebook</button>
      <button type="button" className="btn btn-primary" onClick={this.handleSingout} >Logout</button>
            </div>
        )
    }
}
